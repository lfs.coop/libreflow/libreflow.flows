# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)[^1].

<!---
Types of changes

- Added for new features.
- Changed for changes in existing functionality.
- Deprecated for soon-to-be removed features.
- Removed for now removed features.
- Fixed for any bug fixes.
- Security in case of vulnerabilities.

-->

## [Unreleased]

## [2.6.0] - 2024-11-07

### BREAKING CHANGES

* Force libreflow minimum version to `2.9.0` in order to support Kitsu TV Show projects

### Changed

* Default flow changes when the project is a `TV Show` on Kitsu.
  * A new level to the default asset flow, called asset library.
    * So the structure has the following levels: types, and assets.
    * You can create a asset lib from the kitsu data with the dedicated action.
  * For the default film flow, the film level is used for the episodes.
    * There is also a action for create a film entity from kitsu episodes data.

## [2.5.1] - 2024-06-21

### Removed

* Register the `kabaret.script_view` view type on session, as this is now done automatically in version 1.2.0 of the plugin.

## [2.5.0] - 2024-06-20

### BREAKING CHANGES

* Force libreflow minimum version to `2.6.0` to support replica of a redis cluster.

## [2.4.0] - 2024-06-06

### BREAKING CHANGES

* Force libreflow minimum version to `2.5.0`.
  * All our changes of the Subprocess Manager were applied to the original addon of `kabaret.subprocess_manager`.

### Added

* A command line argument `--show-process-view` as it's called, will show this view of `kabaret.subprocess_manager` when libreflow is started. You can also use it with the `SHOW_PROCESS_VIEW` environment variable by setting it to True.

### Changed

* Clean and harmonise log messages with the session logger for a better readability.
* Kabaret Layout Manager session arguments is updated on the GUI session according to recent changes in Kabaret 2.3.0rc6.

### Fixed

* Create Users From Kitsu: empty list info message is now used.

## [2.3.2] - 2024-05-16

### Changed

* Increased the default height of the assets map in an asset family.
* Activate the filter (search bar) on the assets map.

## [2.3.1] - 2024-05-16

### Added

* New session option to redefine its home oid. This oid can be provided either using the command line argument `--home-oid` or through the environment using the variable `KABARET_HOME_OID`.

## [2.3.0] - 2024-04-26

### Added

The session now uses the new Kabaret's layout manager which is enabled by default with the session autosave feature.

## [2.2.0] - 2024-04-26

### Added

* Flow extensions have been enabled on the entire `default` flow. This feature depends on the [kabaret.flow_extensions](https://gitlab.com/kabaretstudio/kabaret.flow_extensions) module.

## [2.1.2] - 2024-04-26

### Fixed

* Block libreflow minor version to 2.3 to prevent libreflow 2.4.0 or any versions above (introducing breaking changes) from being installed.

## [2.1.1] - 2024-04-26

### Added

* Create Shots, Asset Types and Assets from Kitsu: for existing entities, default tasks are now updated, in case new tasks need to be added.

### Changed

* Improved log messages for create objects from Kitsu actions.

## [2.1.0] - 2024-10-01

### Added

* Creation of shots and assets from Kitsu: an option can be checked to create the task default files enabled by default in the task manager.
* File references are now stored in a collection in the project's entity manager.
* The session provides a new command-line option `--search-auto-indexing` to enable the search engine's automatic indexing. This feature can also be enabled by defining the `SEARCH_AUTO_INDEXING` variable in the environment of the session launch script.

### Fixed

* CompareActions: All sources are now forced to start at frame 1.

### Changed

* Get the tasks to create in shots and assets using the task manager's `get_default_tasks()` method.

## [2.0.10] - 2023-02-13

### Added

* An action to compare any .mp4 or .mov file in a shot task with the shot animatic (by default, `ref_edit.mov` in the `Layout` task).
* An action available allowing to convert any `mov` file to `mp4` format. The user can select the revision to convert. The action then creates a `mp4` file with the same base name, and a revision with the same name.
* Options at the project's root to use libreflow's *My tasks* and *Import Files* tools.
* Access to libreflow's wizard from the Home object.

## [2.0.9] - 2022-11-21

### Added

* An asset type now holds a list of assets (in addition to asset families).
* Asset tasks and their content now appear in the same way as for shots.
* An action allows to copy a rendered image sequence of a given folder. The renamed sequence is created under a new folder with the same name suffixed with `_ok`.

## [2.0.8] - 2022-11-09

### Added

* A new type of asset to gather modules in the default flow.
* Actions for create objects from Kitsu data:
  - Asset Types
  - Assets
  - Sequences
  - Shots
  - Users

### Changed

* The creation of shot tasks uses default tasks with a template named `shot`.

## [2.0.7] - 2022-07-26

### Added

* An action to create a clean-up scene from an animation scene in the `Animation` department. The action uses the last revision of the animation scene, and download it if necessary. When created, the new revision of the clean-up scene is uploaded to the exchange server.
* An asset library to the default flow, with the following levels: types, families and assets. Each asset holds a list of tasks; asset default tasks have their template named `asset` by default.

## [2.0.6] - 2022-06-01

### Changed

* Use `ManagedTask` and `ManagedTaskCollection` (from `libreflow.baseflow.task`) to the configuration and creation of tasks and their files.

## [2.0.5.1] - 2022-05-26

### Fixed

* Define a missing project method returning the project's action value store. It fixes the error raising when the AE playblast rendering dialog shows up and tries to call this method.

## [2.0.5] - 2022-05-17

### Added

* A default flow with the following structure: *film* > *sequence* > *shot* > *task* > *file* > *revision*.
* Whenever a shot is created, its tasks are created according to the default tasks defined in the project's task manager.

## [2.0.3] - 2022-02-04

### Added

* A Blender 3.0 file template for the TinyKin project

### Fixed

* Update of the user's last visit at login

## [2.0.2] - 2022-02-02

### Fixed

* Properly get versions of libreflow and libreflow.flows in order to make them appear on the home page.

## [2.0.1] - 2022-02-02

### Added

* A `Background` department in the shot.

### Changed

* Integrate Libreflow default file feature (2.0.1) to configure and create default files in shot departments.

## [2.0.0] - 2022-01-28

Initial commit for TinyKin project