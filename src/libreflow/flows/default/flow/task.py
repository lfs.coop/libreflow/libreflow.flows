from libreflow.baseflow.task import ManagedTask, ManagedTaskCollection


class Task(ManagedTask):
    pass


class Tasks(ManagedTaskCollection):
    pass
