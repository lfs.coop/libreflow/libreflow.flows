import kabaret.app.resources as resources
import logging


resources.add_folder('file_templates', __file__)
logging.debug('libreflow.flows.tinykin: FILE TEMPLATES LOADED')
